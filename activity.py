
while True:  
    year_input = input("Please enter a year: ")  
    #Check if the input is a positive integer
    if year_input.isdigit() and int(year_input) > 0:  
    
        year = int(year_input)  
        
        if (year % 4 == 0 and year % 100 != 0) or (year % 400 == 0):
            print(f"{year} is a leap year.")
        else:
            print(f"{year} is not a leap year.")
        break  
    else:
        print("Invalid input! Please enter a positive integer for the year.")



# Accept two integer inputs from the user for rows and columns
rows = int(input("Enter the number of rows: "))
columns = int(input("Enter the number of columns: "))

for i in range(rows):
    for j in range(columns):
        print('* ', end='')  
    print()  
